#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <functional>
#include <stdexcept>
#include <sstream>
#include <set>
#include <list>
#include <filesystem>
#include <cstdlib>
#include <regex>

std::map<
	std::string,
	std::map<
		std::string,
		std::function<bool(std::string)>
		>
	> validity_map;
	
std::map<
	std::string,
	std::map<
		std::string,
		std::set<std::string>
		>
	> value_validity_map;
	
std::map<
	std::string,
	std::map<
		std::string,
		std::regex
		>
	> regex_validity_map;
	
std::list<std::string> search_dirs;

std::string current_section, current_variable;



bool defined_values(std::string s)
{
	return value_validity_map[current_section][current_variable].count(s) != 0;
}

bool free_string(std::string p)
{
	return true;
}

bool filename(std::string f)
{
	for(auto & s : search_dirs)
	{
		if(std::filesystem::exists(s+f))
			return true;
	}
	return false;
}

bool shellcmd(std::string sh)
{
	return true;
}

bool number(std::string s)
{
	std::stringstream str(s);
	long long test;
	str >> test;
	if(test == 0 && str.fail())
		return false;
	else
		return true;
}

bool regex(std::string s)
{
	std::regex x = regex_validity_map[current_section][current_variable];
	return std::regex_search(s, x);
}

std::string eval_section(std::string token)
{
	if(token[0] != '[' || token[token.length()-1] != ']')
	{
		std::cout << "ERROR: Wanting section, token isn't embraced with '[' or ']'. Line: " << token << std::endl;
		exit(1);
	}
	else
	{
		return token.substr(1, token.length()-2);
	}
}

// ?????!!!!
bool my_compare(std::string s1, const char* s2)
{
	int end = s1.length()-1;
	for(int i = 0; i < end; i++)
	{
		if(s1[i] != s2[i] || s2[i] == '\0')
			return false;
	}
	if(s2[end] != '\0')
		return false;
	return true;
}

void init_validity_description(std::fstream &description_file)
{
	std::string section;
	int line_no = 0;
	while(description_file.good())
	{
		std::string line;
		std::getline(description_file, line);
		line_no++;
		if(line != "" && line[0] != '#')
		{
			if(line[0] == '[')
			{
				section = eval_section(line);
			}
			else
			{
				std::stringstream buf(line);
				
				// extract variable
				#define token_size 1024*16
				char token_c[token_size];
				buf.getline(token_c, token_size, '\t');
				std::string variable(token_c, buf.gcount());
				if(!buf.good())
				{
					std::cout << "?" << std::endl;
				}
				
				// extract type
				buf.getline(token_c, token_size, '\t');
				std::string type(token_c, buf.gcount());
				
				if(type == "Values" || my_compare(type, "Values"))
				{
					validity_map[section][variable] = defined_values;
					while(buf.good())
					{
						buf.getline(token_c, token_size, '\t');
						std::string token(token_c, buf.gcount());
						if(token != "")
						{
							value_validity_map[section][variable].insert(token);
						}
					}
				}
				else if(type == "String")
				{
					validity_map[section][variable] = free_string;
				}
				else if(type == "Filename")
				{
					validity_map[section][variable] = filename;
				}
				else if(type == "Number")
				{
					validity_map[section][variable] = number;
				}
				else if(type == "ShellCmd")
				{
					validity_map[section][variable] = shellcmd;
				}
				else if(type == "RegEx" || my_compare(type, "RegEx"))
				{
					validity_map[section][variable] = regex;
					while(buf.good())
					{
						buf.getline(token_c, token_size, '\t');
						std::string token(token_c, buf.gcount());
						if(token != "")
						{
							std::regex rx(token, std::regex_constants::ECMAScript);
							regex_validity_map[section][variable] = rx;
						}
					}
				}
				else
				{
					std::cerr << line_no << " unrecognized: " << type;
					std::cerr <<"Values"<< type.compare("Values") << " "
						<<"String"<< type.compare("String") << " "
						<<"Number"<< type.compare("Number") << " "
						<<"Filename"<< type.compare("Filename") << " "
						<<"ShellCmd"<< type.compare("ShellCmd") << " "
						<<"RegEx"<< type.compare("RegEx")
						<< std::endl;
				}
			}
		}
	}
}

std::string remove_leading_spaces(std::string token)
{
	int i = 0;
	while(token[i] == ' ')
		i++;
	return token.substr(i, token.length() - i);
}

std::string remove_trailing_spaces(std::string token)
{
	int i = token.length();
	while(token[i] == ' ')
		i--;
	return token.substr(0, i);
}

std::string remove_surrounding_spaces(std::string token)
{
	int a = 0;
	while(token[a] == ' ')
		a++;
	int b = token.length()-1;
	while(token[b] == ' ')
		b--;
	return token.substr(a, b-a+1);
}

bool test_file(std::fstream &ini_file)
{
	bool ret_val = true;
	int line_no = 0;
	std::string section;
	while(ini_file.good())
	{
		std::string line;
		std::getline(ini_file, line);
		line_no++;
		if(line == "" || line[0] == '#')
			continue;
		if(line[0] == '[')
		{
			section = eval_section(line);
			if(validity_map.count(section) == 0)
			{
				std::cout << "Section " << section << " not found!" << std::endl;
				ret_val = false;
			}
			
		}
		else
		{
			int eq_pos = line.find('=');
			if(eq_pos == std::string::npos)
			{
				std::cout << line_no << ": No '=' found!" << std::endl;
				ret_val = false;
			}
			std::string variable(remove_surrounding_spaces(line.substr(0, eq_pos)));
			std::string contents(remove_surrounding_spaces(line.substr(eq_pos+1)));
			if(validity_map[section].count(variable) != 0)
			{
				current_section = section;
				current_variable = variable;
				if(!validity_map[section][variable](contents))
				{
					ret_val = false;
					std::cout << line_no << " ERROR: [" << section << "], '" << variable << "' = '" << contents << "' invalid!" << std::endl;
				}
			}
			else
			{
				std::cout << line_no << ": Variable '" << variable << "' not found in section '" << section << "'. "<< std::endl;
				ret_val = false;
			}
			
		}
	}
	return ret_val;
}

int main(int argc, char *argv[])
{
	if(argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " test-description-file" << std::endl;
		std::cout << "test-description-file contains lines with descriptions of each test:" << std::endl;
		std::cout << "description-file[TAB]ini-file[TAB]directories-to-search..." << std::endl;
		std::cout << "description-file is a file containing the description of the INI-Format," << std::endl;
		std::cout << "ini-file is the file to test," << std::endl;
		std::cout << "directories-to-search is a tabulator-separated list of directories which are used to verify the existence of a file." << std::endl;
	}
	else
	{
		std::fstream test_descr_file(argv[1], std::ios_base::in);
		while(test_descr_file.good())
		{
			std::string line;
			std::getline(test_descr_file, line);
			if(line == "" || line[0] == '#')
				continue;
			int desc_pos = line.find('\t');
			int ini_pos = line.find('\t', desc_pos+1);
			int search_pos = line.find('\t', ini_pos+1);
			
			// open description and target
			std::string description_filename(line.substr(0, desc_pos));
			std::string target_filename(line.substr(desc_pos+1, ini_pos-desc_pos-1));
			
			std::fstream description_file(description_filename, std::ios_base::in);
			if(!description_file.is_open())
			{
				std::cout << description_filename << " not found!" << std::endl;
			}

			std::fstream target_file(target_filename, std::ios_base::in);
			if(!target_file.is_open())
			{
				std::cout << target_filename << " not found!" << std::endl;
			}
			
			// load directories to search
			search_dirs.clear();
			search_dirs.emplace_back("");
			int i = search_pos;
			while(i != std::string::npos)
			{
				int j = line.find('\t', i);
				if(j != std::string::npos)
					search_dirs.emplace_back(line.substr(i, j-i));
				else
				{
					search_dirs.emplace_back(line.substr(i));
					break;
				}
				i = j+1;
			}
			
			
			validity_map.clear();
			value_validity_map.clear();
			regex_validity_map.clear();
			
			init_validity_description(description_file);
			for(auto &a : validity_map)
			{
				std::cout << "'" << a.first << "'" << std::endl;
				for(auto & b : a.second)
				{
					std::cout << "\t" << "'" << b.first << "'" << std::endl;
				}
			}
			
			std::cout << "Testing " << target_filename << " with " << description_filename << ": " << std::endl;
			
			if(test_file(target_file))
			{
				std::cout << "OK!" << std::endl;
			}
			else
			{
				std::cout << "invalid" << std::endl;
			}
		}
	}

	
	
	return 0;
}

